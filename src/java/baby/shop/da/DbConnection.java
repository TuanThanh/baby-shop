/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baby.shop.da;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Admin
 */
public class DbConnection {
    private static Connection connection;
    
    public static Connection getConnection() throws ClassNotFoundException, SQLException{
        if(connection == null){
            //1. Loading Driver
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            
            //2.Connect
            connection = DriverManager.getConnection("jdbc:derby://localhost:1527/ecommerce", "sa", "123");
        }
        return connection;
    }
    
//    public static void main(String[] args) throws ClassNotFoundException, SQLException {
//        Connection con = getConnection();
//        if(con!=null)
//            System.out.print("connect success");
//    }
}
