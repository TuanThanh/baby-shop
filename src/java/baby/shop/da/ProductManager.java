/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baby.shop.da;

import baby.shop.entity.Product;
import freemarker.log.Logger;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author Admin
 */
public class ProductManager {
    private static PreparedStatement searchByNameStatement;
    private static PreparedStatement searchByIdStatement;
    
    private static PreparedStatement getSearchByNameStatement() throws ClassNotFoundException, SQLException{
        if(searchByNameStatement == null)
        {
            //2. Connect
            Connection connection = DbConnection.getConnection();
            
            //3. Create Statement
            searchByNameStatement = connection.prepareStatement("select id, name, price, description from product where name like ?");
        }
        return searchByNameStatement;
        
    }
    
    private PreparedStatement getSearchByIdStatement() throws ClassNotFoundException, SQLException {
        if(searchByIdStatement == null){
            //2. Connect
            Connection connection = DbConnection.getConnection();
            
            //3. Cteate Statement
            searchByIdStatement = connection.prepareStatement("select name, price, description from product where id = ?");
        }
        return searchByIdStatement;
    }
    
    public static List<Product> getProductsByName(String keyword){
        try{
            PreparedStatement statement = getSearchByNameStatement();
            //4.Process
            statement.setString(1, "%" + keyword + "%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                float price = rs.getFloat("price");
                String description = rs.getString("description");
                products.add(new Product(id, name, price, description));
            }
            return products;
        }catch(Exception ex){
            
            return new LinkedList<Product>();
        }
    }
    
    public Product getProductById(int id){
        try{
            PreparedStatement statement = getSearchByIdStatement();
            //4. Process
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while(rs.next()){
                String name = rs.getString("name");
                float price = rs.getFloat("price");
                String description = rs.getString("description");
                return new Product(id, name , price, description);
            }
        }catch(Exception ex){
            
        }
        return new Product(0, "", 0 , "");
    }
    
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
//        String sqlInsert = "insert into product(name, price, description) values  (?,?,?)";
//        String selectAll = "select id, name, price, description from product";
//        try {
//            // connect to database
//            Class.forName("org.apache.derby.jdbc.ClientDriver");
//            Connection conn = DriverManager.getConnection("jdbc:derby://localhost:1527/ecommerce", "sa", "123");
             
            // crate statement to insert student
//            PreparedStatement stmt = conn.prepareStatement(sqlInsert);
//            stmt.setString(1,"IK");
//            stmt.setString(2, "800");
//            stmt.setString(3, "Test");
//            stmt.execute();
//             
//            // select all student
//            stmt = conn.prepareStatement(selectAll);
//            PreparedStatement stmt = conn.prepareStatement(selectAll);
//            // get data from table 'student'
//            ResultSet rs = stmt.executeQuery();
//            // show data
//            while (rs.next()) {
//                System.out.println(rs.getInt(1) + "  " + rs.getString(2) 
//                        + "  " + rs.getString(3));
//            }
//            
//            
//            stmt.close();
//            conn.close();
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
        
        
//        Connection connection = DbConnection.getConnection();
        
//        PreparedStatement preparedStatement = connection.prepareStatement("select id, name, price, description from product");
//        if(getProductsByName(preparedStatement.toString()) != null){
//            out.println("SUCCESS");
//        }

//          List<Product> list = getProductsByName("IPad");
//          for (Product obj : list) {
//            System.out.println(obj.getName() + "  " + obj.getPrice() 
//                        + "  " + obj.getDescription());
//          }

            
    }
     
    
    
}



